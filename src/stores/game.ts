import type { Unit } from "@/entities";
import { defineStore } from "pinia";
import { ref } from "vue";


export const useGame = defineStore('game', () => {

    const units = ref<Unit[]>([
        {
            hp: 100,
            color: 'red',
            x: 2,
            y: 4,
            type: 'pawn'
        },
        {
            hp: 100,
            color: 'blue',
            x: 10,
            y: 15,
            type: 'pawn'
        }
    ]);

    const selected = ref<Unit>();

    function selectUnit(unit:Unit|undefined) {
        selected.value = unit;
    }

    function moveUnit(x:number,y:number) {
        if(selected.value) {
            selected.value.x = x;
            selected.value.y = y;
        }
    }

    function getUnit(x: number, y: number) {
        return units.value.find(item => item.x == x && item.y == y);
    }

    function addUnit() {
        let x = 0;
        let y = 0;
        while(getUnit(x,y)) {
            y++;
            if(y == 20) {
                y = 0;
                x++;
            }
        }
        units.value.push({
            hp: 100,
            color: 'red',
            x,
            y,
            type: 'pawn'
        });
    }

    return { units, addUnit, getUnit, selected, selectUnit, moveUnit };
});