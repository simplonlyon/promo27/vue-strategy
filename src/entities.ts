export interface Unit {
    x:number;
    y:number;
    type:string;
    hp:number;
    color:string;
}